# Plans

This is where I keep all the plans and ideas I have for this. See the checklist below for what is implemented/todo

## Checklist

- [X] Configuration files
- [ ] Color
- [X] Logo files instead of hardcoded logos
- [X] Fix MacOS "OsRelease"
- [ ] Rebrand to bluefetch
- [ ] Proper documentation
- [X] Fix separator module format.

## Config Checklist
### General
- [X] Default Art
- [X] Art Directory

### Display
- [X] Gap
- [X] Display.TextField

### Display.TextField
- [X] Separator
- [X] Walls
- [X] Gap

### Modules
- [X] Modules
- [X] Definitions

### (Type/Object) Module
- [X] Name
- [X] Key
- [X] Format
- [X] Separator (separator_char)
- [X] Walls
- [X] Module Type
- [X] Execute
